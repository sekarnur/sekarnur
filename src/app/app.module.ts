import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';

import { LoginComponent } from './pages/login/login.component';
import { DataComponent } from './pages/data/data.component';
import { DosenComponent } from './pages/dosen/dosen.component';
import { MatakuliahComponent } from './pages/matakuliah/matakuliah.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    
    LoginComponent,
    DataComponent,
    DosenComponent,
    MatakuliahComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
