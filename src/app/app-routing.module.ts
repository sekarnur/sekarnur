import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';

import { LoginComponent } from './pages/login/login.component';
import { DataComponent } from './pages/data/data.component';
import { DosenComponent } from './pages/dosen/dosen.component';
import { MatakuliahComponent } from './pages/matakuliah/matakuliah.component';

const routes: Routes = [
{
path:'home',
component:HomeComponent
},

{
path:'login',
component:LoginComponent
},
{
path:'data',
component:DataComponent
},
{
path:'dosen',
component:DosenComponent
},
{
path:'matakuliah',
component:MatakuliahComponent
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
